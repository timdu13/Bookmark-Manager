<?php


namespace App\Repositories;


interface DiscussionRepositoryInterface {

    public function save( $args );
    
    public function add_user_discussion( $id_user );
    
    public function remove_user_discussion( $id_user );

}
