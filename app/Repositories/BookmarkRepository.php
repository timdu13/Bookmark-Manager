<?php


namespace App\Repositories;


use App\Bookmark;


class BookmarkRepository implements BookmarkRepositoryInterface {


    protected $bookmark;


    public function __construct(Bookmark $bookmark) {

        $this->bookmark = $bookmark;

    }


    /**
     * Enregistre un Bookmark
     * 
     * @param  array
     * @return mixed bool | App\Bookmark
     */
    public function save( $inputs ) {

        if( ! is_array( $inputs ) ) return false;

        $this->bookmark->name = $inputs['name-bm'];
        $this->bookmark->description = $inputs['description-bm'];
        $this->bookmark->type = $inputs['type-bm'];

        if( $this->bookmark->type === 'text' ) $this->bookmark->content = $inputs['content-bm'];
        elseif( $this->bookmark->type === 'url' ) $this->bookmark->url = $inputs['url-bm'];


        if( $this->bookmark->save() )
            return $this->bookmark;
        else
            return false;

    }
    
    public function countComment() {
        return count( $this->bookmark->comments()->get() );
    }
    
    public static function search( $inputs ) {
        return \Illuminate\Support\Facades\DB::table( 'bookmarks' )->where( 'name', 'like', '%' . $inputs['patern'] . '%' )->get();
    }

}