<?php


namespace App\Repositories;


interface BookmarkRepositoryInterface {

    public function save( $args );

}