<?php


namespace App\Repositories;


use App\Discussion;


class DiscussionRepository implements DiscussionRepositoryInterface {


    protected $discussion;


    public function __construct(Discussion $discussion) {

        $this->discussion = $discussion;

    }


    /**
     * Enregistre une discussion
     * 
     * @param  array
     * @return mixed bool | App\Discussion
     */
    public function save( $inputs ) {

        $this->discussion->name = $inputs[ 'name-dsc' ];

        $this->discussion->group_id = $inputs['group-id'];
        
        $this->discussion->save();
        
        $this->discussion->users()->attach( $inputs['user-id'] );
        
        return $this->discussion;
        
    }
    
    public function add_user_discussion( $id_user ) {
        
        if ( ! $this->discussion->users()->find( $id_user ) ) {
            
            $this->discussion->users()->attach( $id_user );
            $this->discussion->save();
            
        }
        return $this->discussion;
        
    }
    
    
    public function remove_user_discussion( $id_user ) {
        
        if (  $this->discussion->users()->find( $id_user ) ) {
            
            $this->discussion->users()->detach( $id_user );
            $this->discussion->save();
            
        }
        return $this->discussion;
        
    }
    
}

