<?php


namespace App\Repositories;


use App\User;
use App\Bookmark;

use App\Repositories\GroupRepository;
use App\Repositories\DiscussionRepository;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Auth;

class UserRepository implements UserRepositoryInterface {


    protected $user;


    public function __construct(User $user) {

        $this->user = $user;

    }
    
    public function register( $inputs) {
        event(
            new Registered(
                $user = User::create([
                    'name' => $inputs['name'],
                    'email' => $inputs['email'],
                    'password' => bcrypt($inputs['password']),
                ])
            ) 
        );

        $guard = Auth::guard();
        $guard->login($user);
        
        // Ajout de la création de groupe individuel
        $group = new \App\Group();
        $groupRepository = new GroupRepository( $group );
        $groupRepository->save( [
            'name-gp' => $user->name,
            'description-gp' => "Groupe personnel de " . $user->name,
            'visibility-gp' => 'private',
            'user-id' => $user->id,
        ] );
        
        return $user;
    }
    
    
    public function vote_for_bookmark( $id_bookmark ) {
        if( ! $this->user->vote_bookmarks()->find($id_bookmark) ) {
            $bookmark = Bookmark::findOrFail( $id_bookmark );
            $nb_votes = $bookmark->nb_votes;

            if(is_null( $nb_votes ) ) $nb_votes = 0;
            $nb_votes++;
            $bookmark->nb_votes = $nb_votes;
            $bookmark->save();

            
            $this->user->vote_bookmarks()->attach( $id_bookmark );
            return true;
        }
        return false;
        
        
    }
    
    
    public function remove_vote_for_bookmark( $id_bookmark ) {
        if( $this->user->vote_bookmarks()->find($id_bookmark) )
            $this->user->vote_bookmarks()->detach( $id_bookmark );
    }
    
    public function comment_bookmark( $id_bookmark, $content ) {
        $comment = new \App\Comment();
        
        $comment->user_id = $this->user->id;
        $comment->bookmark_id = $id_bookmark;
        $comment->content = $content;
        
        $comment->save();
    }
    
    public static function search( $inputs ) {
        
        return \Illuminate\Support\Facades\DB::table( 'users' )->where( 'name', 'like', '%' . $inputs['patern'] . '%' )->orWhere( 'email', 'like', '%' . $inputs['patern'] . '%')->get();
        
    }
}