<?php


namespace App\Repositories;


use App\Group;
use App\Discussion;
use App\User;

use App\Repositories\DiscussionRepository;

class GroupRepository implements GroupRepositoryInterface {


    protected $group;


    public function __construct(Group $group) {

        $this->group = $group;

    }

    /**
     * Enregistre un group
     * 
     * @param  array
     * @return mixed bool | App\Group
     */
    public function save( $inputs ) {

        $this->group->name = $inputs[ 'name-gp' ];
        $this->group->description = $inputs[ 'description-gp' ];
        $this->group->slug = strtolower( $inputs[ 'name-gp' ] );
        $this->group->visibility = $inputs[ 'visibility-gp' ];

        $this->group->save();

        $this->group->users()->attach( $inputs['user-id'] );
        
        $user = User::findOrFail( $inputs['user-id'] );
        
        // Ajout de la création d'une discussion
        $discussionRepository = new DiscussionRepository( new Discussion );
        $discussionRepository->save( [
            'name-dsc' => 'Première discussion de '. $this->group->name,
            'group-id' => $this->group->id,
            'user-id' => $user->id
        ] );
        
        return $this->group;

    }
    
    public static function search( $inputs ) {
        return \Illuminate\Support\Facades\DB::table( 'groups' )->where( 'name', 'like', '%' . $inputs['patern'] . '%' )->orWhere( 'description', 'like', '%' . $inputs['patern'] . '%')->get();
    }

}