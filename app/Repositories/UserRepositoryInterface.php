<?php

namespace App\Repositories;

interface UserRepositoryInterface {

    public function vote_for_bookmark( $id_bookmark );
    public function remove_vote_for_bookmark( $id_bookmark );
    public static function search( $inputs );
}
