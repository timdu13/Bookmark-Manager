<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model {
    
    public function bookmark() {
        
        return $this->belongsTo('App\Bookmark');
        
    }
    
    public function user() {
        
        return $this->belongsTo('App\User');
        
    }
    
    public function getCreatedAtAttribute( $date ) {
        $date = new \Carbon\Carbon( $date );
        
        return $date->format( 'd/m/Y à H:i' );
    }
    
}
