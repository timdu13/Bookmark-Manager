<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Discussion extends Model {
    
    public function messages() {
        
        return $this->belongsToMany( 'App\Message', 'discussion_message', 'discussion_id', 'message_id' );
        
    }
    
    public function users() {
        
        return $this->belongsToMany('App\User', 'discussion_user', 'discussion_id', 'user_id');
        
    }
    
    public function group() {
        
        return $this->belongsTo('App\Group');
        
    }
    
    /**
     * Verifie si $id_discussion contient des messages
     * @param int $id_discussion
     * @return bool
     */
    public static function isEmpty( $id_discussion ) {
        
        $discussion = Discussion::findOrFail( $id_discussion );
        $nb_messages = count( $discussion->messages()->get() );
        
        $discussion_status = ( 0 === $nb_messages ) ? true : false;
        
        return $discussion_status;
    }
    
    /**
     * Verifie si $id_user est dans $id_discussion
     * @param int $id_user
     * @param int $id_discussion
     * @return bool
     */
    public static function isInDiscussion( $id_user, $id_discussion ) {
        
        $discussion = Discussion::findOrFail( $id_discussion );
        
        $user = $discussion->users()->where('id', $id_user)->first();
        
        $in_discussion = ( !is_null($user) ) ? true : false;
        
        return $in_discussion;
        
    }
}
