<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bookmark extends Model {
    
    protected $fillable = ['name'];
    
    public function taxonomies() {
        
        return $this->belongsToMany('App\Taxonomy', 'bookmark_taxonomy', 'bookmark_id', 'taxonomy_id');
        
    }
    
    public function comments() {
        
        return $this->hasMany('App\Comment', 'bookmark_id');
        
    }
    
    public function user() {
        
        return $this->belongsTo('App\User');
        
    }
    
    public function group() {
        
        return $this->belongsTo('App\Group');
        
    }
    
    public function votes() {
        
        return $this->belongsToMany( 'App\User', 'user_vote', 'bookmark_id', 'user_id' );
        
    }
}
