<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DiscussionMessage extends Model {
    protected $table = 'discussion_message';
}
