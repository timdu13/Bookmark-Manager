<?php

namespace App\Http\Controllers;

use \App\Http\Requests\SearchRequest ;
use Illuminate\Http\Request;


use \App\Repositories\GroupRepository;

use App\Group;
use App\User;
use Auth;



class GroupController extends Controller {
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        
        $this->authorize( 'lister', Group::class );
        
        $groups = Group::all();
        
        return view( 'list.group' )->with( 'groups', $groups );
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        
        return view('form.edit-gp');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        
        $user = Auth::user();
        
        if ( $user ) {
            
            $inputs = array_merge( $request->all(), [ 'user-id' => $user->id ] );
            $groupRepository = new GroupRepository( new Group );
            $group = $groupRepository->save( $inputs );
        
        }
        else {
            return redirect( route('login') );
        }
        
        return redirect( route('group-edit', $group->id) );

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        
        $group = Group::findOrFail($id);
        
        return view( 'single.group', [ 'group' => $group, 'canEdit' => true ] );
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        
        $group = Group::findOrFail($id);
        
        $this->authorize( 'update', $group );
        
        return view( 'form.edit-gp', [ 'group' => $group ] );
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, $id_user = NULL) {
        
        if ( isset( $id_user ) ) {
            
            $group = Group::findOrFail( $id );
            $group->users()->detach( $id_user );
            $group->save();
        }
        else {
        
            $group = Group::findOrFail( $id );
            $group->name = $request->input('name-gp');
            $group->description = $request->input('description-gp');
            $group->visibility = $request->input('visibility-gp');
            
            $group->save( $request->all() );
            
        }
        
        return redirect( route( 'group-edit', $id ) );
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        
        $this->authorize( 'update', Group::findOrFail( $id ) );
        
        Group::destroy( $id );
        
        return redirect( route('group-list') );
        
    }
    
    public function search( SearchRequest $request  ) {
        $groups = GroupRepository::search( $request->all() );
        
        return view( 'list.group' )->with( 'groups', $groups );
    }
    
}
