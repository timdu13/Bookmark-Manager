<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Auth;
use App\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        
        $auth = Auth::user();
        
        if ( $auth ) {
        
            $users = User::all();
            return view( 'list.user', [ 'users' => $users ] );
        }
        return redirect( route( 'login' ) );
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        
        $auth = Auth::user();
        
        if ( $auth ) {
        
            $user = User::findOrFail( $id );
            return view( 'single.user', [ 'user' => $user ] );
        
        }
        return redirect( route( 'login' ) );
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        
        $auth = Auth::user();
        
        if ( $auth ) {
        
            $user = User::findOrFail( $id );
            return view( 'edit.user', [ 'user' => $user ] );
        
        }
        return redirect( route( 'login' ) );
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        
        $auth = Auth::user();
        
        if ( $auth ) {
        
            $user = User::findOrFail($id);
            $user->save( $request->all() );
            return redirect( route( 'user-edit', $id) );
            
        }
        return redirect( route( 'login' ) );
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        
        $auth = Auth::user();
        
        if ( $auth ) {
        
            User::destroy( $id );
        
        }
        return redirect( route( 'login' ) );
        
    }
}
