<?php

namespace App\Http\Controllers;


use Auth;
use App\Discussion;
use App\Group;
use App\User;

use App\Repositories\UserRepository;
use App\Repositories\DiscussionRepository;

use Illuminate\Http\Request;
use \App\Http\Requests\SearchRequest ;

class DiscussionController extends Controller
{
    
    public function searchusers( SearchRequest  $request, $id ) {
        
        $discussion = Discussion::findOrFail( $id );
        $users = UserRepository::search( $request->all() );
        $group = Group::findOrFail($discussion->id);
        return  view( 'form.edit-dsc' , [ 'discussion' => $discussion, 'users' => $users, 'group' => $group ] );
        
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id_group) {
        
        $user = Auth::user();
        
        if ( $user ) {
            
            $group = Group::findOrFail( $id_group );
            $group_discussions = $group->discussion()->get();
            
            return view( 'list.discussion', [ 'discussions' => $group_discussions ] );
            
        }
        return redirect( route( 'login' ) );
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        
        $users = User::all();
        
        return view( 'form.edit-dsc', [ 'users' => $users ] );
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        
        $user = Auth::user();
        
        if ( $user ) {
            
            $inputs = array_merge( $request->all(), [ 'group-id' => $user->id, 'user-id' => $user->id ] );
            $discussionRepository = new DiscussionRepository( new Discussion );
            $discussion = $discussionRepository->save( $inputs );
            return redirect( route('discussion-edit', $discussion->id ) );
        }
        return redirect( route('login') );
        
        
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        
        $user = Auth::user();
        
        if ( $user ) {
            
            $discussion = Discussion::findOrFail($id);
            return view( 'single.discussion', [ 'discussion' => $discussion ] );
            
        }
        return redirect( route('login') );
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        
        $user = Auth::user();
        
        if ( $user ) {
            
            $discussion = Discussion::findOrFail($id);
            
            $group = Group::findOrFail($discussion->id);
            
            return view( 'form.edit-dsc', [ 'discussion' => $discussion, 'group' => $group ] ); 
            
        }
        return redirect( route('login') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update( Request $request, $id ) {
        
        $user = Auth::user();
        
        if ( $user ) {
        
            $discussion = Discussion::findOrFail($id);
            $discussion->save( $request->all() );
            return redirect( route( 'discussion-edit', $id) );
        }
        return redirect( route('login') );
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        
        $user = Auth::user();
        
        if ( $user ) {
            
            Discussion::destroy($id);
            
            return redirect( route('discussion-list') );
            
        }
        return redirect( route('login') );
        
    }
    
    /**
     * Liste les users à ajouter à la discussion
     * @param int $id_discussion
     * @return view
     */
    public function index_user_discussion( $id_discussion ) {
        
        $discussion = Discussion::findOrFail( $id_discussion );
        $users = User::all();
        
        return view( 'list.users-discussion', [ 'discussion' => $discussion, 'users' => $users ] );
        
        
    }
    
    /**
     * Permet d'ajouter un user à une discussion
     * @param int $id_user
     * @param int $id_discussion
     * @return view
     */
    public function add_user_discussion( $id_user, $id_discussion ) {
       
        $discussion = Discussion::findOrFail( $id_discussion );
        $discussionRepository = new DiscussionRepository( $discussion );
        $discussionRepository->add_user_discussion( $id_user );
        
        return redirect( route( 'list-users-discussion', $id_discussion ) );
        
    }
    
    /**
     * Permet de supprimer une utilisateur d'une discussion
     * @param int $id_user
     * @param int $id_discussion
     * @return type
     */
    public function remove_user_discussion( $id_user, $id_discussion ) {
        
        $discussion = Discussion::findOrFail( $id_discussion );
        $discussionRepository = new DiscussionRepository( $discussion );
        $discussionRepository->remove_user_discussion( $id_user );
        
        return redirect( route( 'list-users-discussion', $id_discussion ) );
        
    }
}
