<?php

namespace App\Http\Middleware;

use Closure;
use App\Group;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        $user = $request->user();
        if( ! is_null( $user ) ) return redirect( route('group-list') );
        
        $group = Group::findOrFail( $request->route()->id );
        
        
        if ( $group->isAdmin( $user->id ) || $user->role === 'admin' ) {
        
            return $next($request);
            
        }
        return redirect( route('group-list') );
    }
}
