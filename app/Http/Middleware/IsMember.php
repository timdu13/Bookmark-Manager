<?php

namespace App\Http\Middleware;

use Closure;

class IsMember
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle( $request, Closure $next ) {
        $user = $request->user();
        if( ! is_null( $user ) ) return redirect( route('group-list') );
        
        $group = Group::findOrFail( $request->route()->id );
        
        if ( $group->isMember( $user->id ) ) {
        
            return $next($request);
            
        }
        return redirect( route('group-list') );

    }
}
