<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Taxonomy extends Model {
    
    public function bookmarks() {
        
        return $this->belongsToMany('App\Bookmark', 'bookmark_taxonomy', 'taxonomy_id', 'bookmark_id');
    
    }
    
    public function users() {
        
        return $this->belongsToMany('App\User', 'favorite_taxonomy', 'taxonomy_id', 'user_id');
        
    }
    
    public function categories_child() {
        return DB::table( 'taxonomies' )->where( 'parent', '=', $this->id )->get();
    }
   
}
