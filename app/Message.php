<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model {

    public function discussions() {
        
        return $this->belongsToMany( 'App\Discussion', 'discussion_message', 'message_id', 'discussion_id' );
        
    }
    
    public function user() {
        
        return $this->belongsTo( 'App\User' );
        
    }
    
}
