<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    public function bookmarks() {
        
        return $this->hasMany( 'App\Bookmark' );
        
    }
    
    public function discussions() {
        
        return $this->belongsToMany( 'App\Discussion', 'discussion_user', 'user_id', 'discussion_id' );
        
    }
    
    public function groups() {
        
        return $this->belongsToMany( 'App\Group', 'group_user', 'user_id', 'group_id' );
        
    }
    
    public function notifications() {

        return $this->hasMany('App\Notification');
        
    }
    
    public function comments() {
        
        return $this->hasMany('App\Comment');
        
    }
    
    public function messages() {
        
        return $this->hasMany('App\Message');
        
    }
    
    public function taxonomies() {
        
        return $this->belongsToMany('App\Taxonomy', 'favorite_taxonomy', 'user_id', 'taxonomy_id');
        
    }
    
    public function vote_bookmarks() {
        
        return $this->belongsToMany( 'App\Bookmark', 'user_vote', 'user_id', 'bookmark_id' );
        
    }
    
    public function groups_admin() {
        
        return $this->belongsToMany('App\Group', 'group_user', 'user_id', 'group_id')->wherePivotIn('role', ['admin']);
        
    }
    
    public function groups_member() {
        
        return $this->belongsToMany('App\Group', 'group_user', 'user_id', 'group_id')->wherePivotIn('role', ['member']);
        
    }
    
    public function groups_follower() {
        
        return $this->belongsToMany('App\Group', 'group_user', 'user_id', 'group_id')->wherePivotIn('role', ['follower']);
        
    }
    
    public function isAdmin() {
        return $this->role === 'admin';
    }
    
}

