<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DiscussionUser extends Model {
    protected $table = 'discussion_user';
}
