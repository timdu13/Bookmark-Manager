<?php

namespace App\Policies;

use App\User;
use App\Group;
use Illuminate\Auth\Access\HandlesAuthorization;

class GroupPolicy
{
    use HandlesAuthorization;

    public function before($user, $ability) {
        if ( $user->isAdmin() ) {
            return true;
        }
    }
    
    public function lister( User $user ){
        return $user->isAdmin();
    }
    
    /**
     * Determine whether the user can view the appGroup.
     *
     * @param  \App\User  $user
     * @param  \App\Group  $group
     * @return mixed
     */
    public function view(User $user, Group $group)
    {
        //
    }

    /**
     * Determine whether the user can create appGroups.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user) {
        return true;
    }

    /**
     * Determine whether the user can update the appGroup.
     *
     * @param  \App\User  $user
     * @param  \App\Group  $group
     * @return mixed
     */
    public function update(User $user, Group $group) {
        return $group->isAdmin( $user->id );
    }

    /**
     * Determine whether the user can delete the appGroup.
     *
     * @param  \App\User  $user
     * @param  \App\Group  $group
     * @return mixed
     */
    public function delete(User $user, Group $group) {
        return $group->isAdmin( $user->id );
    }
}
