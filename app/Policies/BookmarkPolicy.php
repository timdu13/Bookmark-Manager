<?php

namespace App\Policies;

use App\User;
use App\Bookmark;
use Illuminate\Auth\Access\HandlesAuthorization;

class BookmarkPolicy {
    use HandlesAuthorization;

    public function before($user, $ability) {
        if ( $user->isAdmin() ) {
            return true;
        }
    }

    /**
     * Determine whether the user can view the Bookmark.
     *
     * @param  \App\User  $user
     * @param  \App\Bookmark  $bookmark
     * @return mixed
     */
    public function view(User $user, Bookmark $bookmark) {
        //
    }
    
    public function lister( User $user ) {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can create appBookmarks.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user) {
        return true;
    }

    /**
     * Determine whether the user can update the appBookmark.
     *
     * @param  \App\User  $user
     * @param  \App\Bookmark  $bookmark
     * @return mixed
     */
    public function update(User $user, Bookmark $bookmark){
        return $user->id === $bookmark->user_id;
    }

    /**
     * Determine whether the user can delete the appBookmark.
     *
     * @param  \App\User  $user
     * @param  \App\Bookmark  $bookmark
     * @return mixed
     */
    public function delete(User $user, Bookmark $bookmark) {
        return $user->id === $bookmark->user_id;
    }
}
