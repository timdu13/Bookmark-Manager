<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

// Users
Route::get( 'list/users', 'UserController@index' )->name('user-list');
Route::get( 'single/user/{id}', 'UserController@show' )->name('user-single');
Route::get( 'update/user/{id}', 'UserController@edit' )->name('user-edit');


Route::get('/', function () {
    return view('welcome');
});

// Bookmarks
Route::get( '/list/bookmarks', 'BookmarkController@index' )->name('bookmark-list')->middleware( 'auth' );
Route::get( '/new/bookmark', 'BookmarkController@create' )->name('bookmark-create')->middleware( 'auth' );
Route::post( '/new/bookmark', 'BookmarkController@store' )->name('bookmark-store')->middleware( 'auth' );
Route::get( '/update/bookmark/{id}', 'BookmarkController@edit' )->name('bookmark-edit')->middleware( 'auth' );
Route::put( '/update/bookmark/{id}', 'BookmarkController@update' )->name('bookmark-update')->middleware( 'auth' );
Route::delete( '/delete/bookmark/{id}', 'BookmarkController@destroy' )->name('bookmark-destroy')->middleware( 'auth' );
Route::get( '/single/bookmark/{id}', 'BookmarkController@show' )->name('bookmark-single');
Route::post( '/addcomment/bookmark/{id}', 'BookmarkController@add_comment' )->name( 'bookmark-add-comment' )->middleware( 'auth' );
Route::post( '/addvote/bookmark', 'BookmarkController@add_vote' )->name('bookmark-add-vote');
Route::post( '/search/bookmark', 'BookmarkController@search')->name('bookmark-search');

// Groups
Route::get( '/list/groups', 'GroupController@index' )->name('group-list')->middleware( 'auth' );
Route::get( '/new/group', 'GroupController@create' )->name('group-create')->middleware( 'auth' );
Route::post( '/new/group', 'GroupController@store' )->name('group-store')->middleware( 'auth' );
Route::get( '/update/group/{id}', 'GroupController@edit' )->name('group-edit')->middleware( 'auth' );
Route::put( '/update/group/{id}', 'GroupController@update' )->name('group-update')->middleware( 'auth' );
Route::delete( '/delete/group/{id}', 'GroupController@destroy' )->name('group-destroy')->middleware( 'auth' );
Route::get( '/single/group/{id}', 'GroupController@show' )->name( 'group-single' )->middleware( 'auth' );
Route::delete( '/remove-user/group/{id}/user/{id_user}', 'GroupController@update' )->name( 'group-remove-user' )->middleware( 'auth', 'IsAdmin' );
Route::post( '/search/group', 'GroupController@search')->name('group-search');

// Discussions
Route::get( 'list/discussions/group/{id_group}', 'DiscussionController@index' )->name('discussion-list')->middleware( 'auth' );
Route::get( 'new/discussion', 'DiscussionController@create' )->name('discussion-create')->middleware( 'auth' );
Route::post( 'new/discussion', 'DiscussionController@store' )->name('discussion-store')->middleware( 'auth' );
Route::get( 'update/discussion/{id}', 'DiscussionController@edit' )->name('discussion-edit')->middleware( 'auth' );
Route::put( 'update/discussion/{id}', 'DiscussionController@update' )->name('discussion-update')->middleware( 'auth' );
Route::get( 'single/discussion/{id}', 'DiscussionController@show' )->name('discussion-show')->middleware( 'auth' );
Route::delete( 'delete/discussion/{id}', 'DiscussionController@destroy' )->name('discussion-destroy')->middleware( 'auth' );
Route::post( 'search/users/discussion/{id}', 'DiscussionController@searchusers' )->name('search-users-discussion');
Route::post( 'add/user/{id_user}/discussion/{id_discussion}', 'DiscussionController@add_user_discussion' )->name('add-user-discussion');
Route::delete( 'remove/user/{id_user}/discussion/{id_discussion}', 'DiscussionController@remove_user_discussion' )->name('remove-user-discussion');
Route::get( 'list/users/discussions/{id}', 'DiscussionController@index_user_discussion' )->name('list-users-discussion');


// Messages
Route::get( 'list/messages', 'MessageController@index' )->name('message-list')->middleware( 'auth' );
Route::get( 'new/message', 'MessageController@create' )->name('message-create')->middleware( 'auth' );
Route::post( 'new/message/discussion/{id}', 'MessageController@store' )->name('message-store')->middleware( 'auth' );
Route::get( 'single/message/{$id}', 'MessageController@show' )->name('message-signle')->middleware( 'auth' );
Route::delete( 'delete/message/{$id}', 'MessageController@destroy' )->name('message-destroy')->middleware( 'auth' );
