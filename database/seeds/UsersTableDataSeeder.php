<?php

use Illuminate\Database\Seeder;

class UsersTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        
        for( $i = 0; $i < 10; $i++ ) {
            
            DB::table('users')->insert([
                'name' => 'user '.$i,
                'email' => 'mailuser'. $i .'@hotmail.fr',
                'password' => bcrypt('1995'),
                'role' => 'member'
            ]);
            
        }
        
    }
}
