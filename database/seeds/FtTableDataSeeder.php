<?php

use Illuminate\Database\Seeder;

class FtTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        
        for ( $i = 0; $i < 10; $i++ ) {
            
            DB::table('favorite_taxonomy')->insert([
                
                'user_id' => $i,
                'taxonomy_id' => $i
                
            ]);
            
        }
        
    }
}
