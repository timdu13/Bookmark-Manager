<?php

use Illuminate\Database\Seeder;

class MessagesTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        for ( $i = 0; $i < 10; $i++ ) {
            
            DB::table('messages')->insert([
                
                'subject' => 'Sujet : '.$i,
                'user_id' => rand(1, 10),
                'discussion_id' => rand(1, 5),
                'message' => str_random(20)
                
            ]);
            
        }
        
    }
}
