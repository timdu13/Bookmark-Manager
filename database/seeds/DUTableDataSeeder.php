<?php

use Illuminate\Database\Seeder;

class DUTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        
        for ( $i = 0; $i < 10; $i++ ) {
            
            DB::table('discussion_user')->insert([
                
                'discussion_id' => rand(1, 5),
                'user_id' => rand(1, 10)
                
            ]);
            
        }
        
    }
}
