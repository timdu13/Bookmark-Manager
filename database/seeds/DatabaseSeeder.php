<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        
        $this->call('UsersTableDataSeeder');
        $this->call('TaxoTableDataSeeder');
        $this->call('BookmarkTableDataSeeder');
        $this->call('BookTaxoTableDataSeeder');
        $this->call('CommentsTableDataSeeder');
        $this->call('DiscussionsTableDataSeeder');
        $this->call('DMTableDataSeeder');
        $this->call('DUTableDataSeeder');
        $this->call('MessagesTableDataSeeder');
        $this->call('FtTableDataSeeder');
        $this->call('GroupTableDataSeeder');
        $this->call('GUTableDataSeeder');
        $this->call('NotifTableDataSeeder');
        
    }
}
