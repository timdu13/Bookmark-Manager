<?php

use Illuminate\Database\Seeder;

class GUTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        for ( $i = 0; $i < 5; $i++ ) {
            
            DB::table('group_user')->insert([
               
                'user_id' => rand(1, 10),
                'group_id' => $i,
                
            ]);
            
        }
        
        for ( $i = 5; $i < 10; $i++ ) {
            
            DB::table('group_user')->insert([
               
                'user_id' => rand(1, 10),
                'group_id' => $i,
                
            ]);
            
        }
        
    }
}
