<?php

use Illuminate\Database\Seeder;

class TaxoTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ( $i = 0; $i < 5; $i++ ) {
            
            DB::table('taxonomies')->insert([
                
                'type' => 'category',
                'name' => 'categorie '.$i,
                'slug' => 'categorie-'.$i
                
            ]);
            
        }
        
        for ( $i = 5; $i < 10; $i++ ) {
            
            DB::table('taxonomies')->insert([
                
                'type' => 'tag',
                'name' => 'tag '.$i,
                'slug' => 'tag-'.$i
                
            ]);
            
        }
    }
}
