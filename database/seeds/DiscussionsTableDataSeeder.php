<?php

use Illuminate\Database\Seeder;

class DiscussionsTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        
        for ( $i = 0; $i < 5; $i++ ) {
            
            DB::table('discussions')->insert([
                
               'name' => 'Discussion '.$i,
               'message_id' => $i,
                
            ]);
            
        }
        
    }
}
