<?php

use Illuminate\Database\Seeder;

class BookTaxoTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        
        for ( $i = 0; $i < 10; $i++ ) {
            
            DB::table('bookmark_taxonomy')->insert([
               
                'taxonomy_id' => rand(1, 10),
                'bookmark_id' => rand(1, 10)
                
            ]);
            
        }
        
    }
}
