<?php

use Illuminate\Database\Seeder;

class GroupTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        
        for ( $i = 0; $i < 5; $i++ ) {
            
            DB::table('groups')->insert([
                
               'name' => 'Groupe : '.$i,
               'description' => 'Description du groupe numero :'. $i,
               'slug' => 'groupe-'.$i,
               'visibility' => 'public'
                
            ]);
            
        }
        
        for ( $i = 5; $i < 10; $i++ ) {
            
            DB::table('groups')->insert([
                
               'name' => 'Groupe : '.$i,
               'description' => 'Description du groupe numero :'. $i,
               'slug' => 'groupe-'.$i,
               'visibility' => 'private'
                
            ]);
            
        }
        
    }
}
