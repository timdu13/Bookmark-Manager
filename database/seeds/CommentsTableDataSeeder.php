<?php

use Illuminate\Database\Seeder;

use Faker\Factory;

class CommentsTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $faker = Factory::create();
        for ( $i = 0; $i < 10; $i++ ) {
            
            DB::table('comments')->insert([
                
               'user_id' => rand(1, 10),
               'bookmark_id' => rand(1, 10),
               'content' => $faker->realText( 350 )
                
            ]);
            
        }
         
    }
}
