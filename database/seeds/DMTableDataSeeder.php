<?php

use Illuminate\Database\Seeder;

class DMTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        for ( $i = 0; $i < 5; $i++ ) {
            
            DB::table('discussion_message')->insert([
               
                'message_id' => rand(1, 10),
                'discussion_id' => $i
                
            ]);
            
        }
        
    }
}
