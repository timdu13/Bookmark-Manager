@extends('template')


@section('content')





<div class="panel panel-primary">
    <div class="panel-heading">
        <h2> Liste des bookmarks du site </h2>
    </div>
    
    <div class="panel-body">
        <ul>
            {!! display_cats_child( $cat ) !!}
        </ul>
    </div>
</div>

		
@stop
@section('scripts')

<script>

    $(".btn-arboressence").on("click", function(){
        $(this).next().fadeToggle();
        
        if ( $(this).children( ".glyphicon" ).hasClass( 'glyphicon-chevron-down' ) ) {
            $(this).children( ".glyphicon" ).removeClass("glyphicon-chevron-down").addClass("glyphicon-chevron-right");
        } else {
            $(this).children( ".glyphicon" ).removeClass("glyphicon-chevron-right").addClass("glyphicon-chevron-down");
        }
    });

</script>

@stop

@section('styles')
<style>
        li {
            list-style: none;
        }
        .btn-arboressence {
            cursor: pointer;
        }
        
        .li-bm {
            list-style: disc;
        }
    </style>
@stop


<?php

function display_cats_child( $cat ) {
    $bookmarks = $cat->bookmarks()->get();

    // On n'affiche pas la catégorie si elle n'a pas de bookmark enfant
    if( count( $bookmarks ) == 0 && $cat->name != "categorie 0" ) return;
    
    $cat_childs = $cat->categories_child();
?>

<li>
        <div class='btn-arboressence'>
            <span class='glyphicon glyphicon-chevron-down'></span>
            {{ $cat->name }}
        </div>
        
    <ul>
            <?php // On mets d'abord les catégorie enfantes si il en existe
            if( count( $cat_childs ) > 0 ) {
                foreach( $cat_childs as $cat_child ) {
                    $cat_child = App\Taxonomy::findOrFail( $cat_child->id );
                    display_cats_child( $cat_child );
                }
            }
            // et ensuite les bookmark qu'elle contient
            
            foreach( $bookmarks as $bookmark ): ?>
                <li class='li-bm'>
                    <a href='{{ route( 'bookmark-single', $bookmark->id ) }}'>{{ $bookmark->name }}</a>
                </li>
            <?php endforeach; ?>
        </ul>
    </li>
<?php
}
?>

