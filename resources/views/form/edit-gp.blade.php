@extends('template')


@section('content')
    
<?php 
if( isset($group) ) {
    $type = "Mettre à jour";
    $options = [ 'method' => 'PUT', 'route' => [ 'group-update', $group->id ] ];

}
else {

    $group = new \App\Group;

    $type = "Ajouter";
    $options = ['method' => 'POST', 'route' => [ 'group-create' ] ];

}
?>

@include('errors')

    <div class="panel panel-default">

        <div class="panel-heading">{{ $type }} un groupe</div>


        <div class="panel-body">
            {!! Form::open($options) !!}
            
            <div class="form-group">
                {{ Form::label('name-gp', "Nom du Groupe", ['class' => 'control-label']) }}
                {{ Form::text('name-gp', $group->name,  ['class' => 'form-control']) }}
            </div>
            
            <div class="form-group">
                {{ Form::label('description-gp', "Description", ['class' => 'control-label']) }}
                {{ Form::text('description-gp', $group->description,  ['class' => 'form-control']) }}
            </div>
            
            <div class="form-group">
                {{ Form::label('visibility-gp', "Visibilité", ['class' => 'control-label']) }}
                {{ Form::select( 'visibility-gp',  [ 'public' => 'Publique', 'private' => 'Privé' ], $group->visibility, ['class' => 'form-control'] ) }}
            </div>

            @if ( $type === "Mettre à jour" )
                
                <div class="form-group">
                    {{ Form::submit('Mettre à jour', ['class' => 'btn btn-primary']) }}
                </div>
            
            @else 
            
                <div class="form-group">
                    {{ Form::submit('Ajouter', ['class' => 'btn btn-primary']) }}
                </div>
            
            @endif
            
            {!! Form::close() !!}
            
            @if( $group->users()->get()->count() > 0 )
            <table class="table table-hover bg-default">
                <caption>Liste des users du groupe</caption>
                    <tr>
                        <th>Nom</th>
                        <th>Rôle</th>
                        <th>Action</th>
                    </tr>
                @foreach( $group->users as $user )
                        <tr>
                            <!-- Ajouter la route single-user -->
                            <td> <a class="col-md-10" href="#"> {{ $user->name }}</a> </td>
                                <td class="td">{{ $group->role_user_group( $group->users->first()->id ) }}</td>
                                <td>
                                        {!! Form::open([ 'method' => 'DELETE', 'route' => ['group-remove-user', $group->id, $user->id ]]) !!}
                                        {!! Form::submit('Supprimer', ['class' => 'btn btn-danger', 'aria-hidden' => 'true', 'onclick' => 'return confirm(\'Vraiment supprimer cet utilisateur ?\')']) !!}
                                        {!! Form::close() !!}
                                </td>
                        </tr>
                @endforeach
                
            </table>
            @elseif( $type === 'Mettre à jour' )
            <div class="alert alert-danger" role="alert">Le groupe de contient pas d'utilisateur !</div>
            @endif
            
            @if( $group->bookmarks()->get()->count() > 0 )
            <table class="table table-hover bg-default">
                <caption>Liste des boomarks du groupe</caption>
                    <tr>
                        <th>Nom</th>
                        <th>Description</th>
                        <th>Auteur</th>
                        <th>Action</th>
                    </tr>
                @foreach( $group->bookmarks as $bookmark )
<?php //dd( $bookmark->id ) ?>
                        <tr>
                            <td> <a class="col-md-10" href="{{ route('bookmark-single' , $bookmark->id) }}"> {{ $bookmark->name }}</a> </td>
                                <td class="td">{{ $bookmark->description }}</td>
                                <td class="td">{{ $bookmark->user()->first()->name }}</td>
                                <td>
                                        {!! Form::open([ 'method' => 'DELETE', 'route' => ['bookmark-destroy', $bookmark->id ]]) !!}
                                        {!! Form::submit('Supprimer', ['class' => 'btn btn-danger', 'aria-hidden' => 'true', 'onclick' => 'return confirm(\'Vraiment supprimer ce bookmark ?\')']) !!}
                                        {!! Form::close() !!}
                                </td>
                        </tr>
                @endforeach
                
            </table>
            @elseif( $type === 'Mettre à jour' )
            <div class="alert alert-warning" role="alert">Le groupe de contient pas encore de bookmark</div>
            @endif
        </div>
    </div>
@stop

