@extends('template')

<?php

if( isset($discussion) ) {
    $type = "Mettre à jour";
    $options = [ 'method' => 'PUT', 'route' => [ 'discussion-update', $discussion->id ] ];

}
else {

    $discussion = new \App\Discussion;

    $type = "Ajouter";
    $options = ['method' => 'POST', 'route' => [ 'discussion-create' ] ];

}

$patern = ( isset($patern) ) ? $patern : '';
?>

@include('errors')

@section('content')

    <div class="panel panel-default">

        <div class="panel-heading">{{ $type }} une discussion</div>

        <!-- Inputs -->
        <div class="panel-body">
            {!! Form::open($options) !!}
            
            <div class="form-group">
                {{ Form::label('name-dsc', "Nom de la discussion", ['class' => 'control-label']) }}
                {{ Form::text('name-dsc', $discussion->name,  ['class' => 'form-control']) }}
            </div>
            
            @if ( $type === "Mettre à jour" )
                
                <div class="form-group">
                    {{ Form::submit('Mettre à jour', ['class' => 'btn btn-primary']) }}
                </div>
            
            @else 
            
                <div class="form-group">
                    {{ Form::submit('Ajouter', ['class' => 'btn btn-primary']) }}
                </div>
            
            @endif
            
            {!! Form::close() !!}
            
            @if ( "Mettre à jour" === $type )
            
                <table class="table table-hover bg-default">
                    <caption>Liste des users de la discussion</caption>
                    
                        <tr>
                            <th>Nom</th>
                            <th>Rôle</th>
                            <th>Action</th>
                        </tr>
                        
                        @foreach( $discussion->users()->get() as $user )
                            <tr>
                                <!-- Ajouter la route single-user -->
                                <td> <a class="col-md-10" href="#"> {{ $user->name }}</a> </td>
                                    <td class="td">{{ $group->role_user_group( $group->users->first()->id ) }}</td>
                                    <td>
                                            {!! Form::open([ 'method' => 'DELETE', 'route' => ['remove-user-discussion', $user->id, $discussion->id ]]) !!}
                                            {!! Form::submit('Supprimer', ['class' => 'btn btn-danger', 'aria-hidden' => 'true', 'onclick' => 'return confirm(\'Vraiment supprimer cet utilisateur de la discussion ?\')']) !!}
                                            {!! Form::close() !!}
                                    </td>
                            </tr>
                        @endforeach
                    
                    @if ( isset($users) )
                        
                        @foreach( $users as $user ) 
                                <tr>
                                    <!-- Ajouter la route single-user -->
                                    <td> <a class="col-md-10" href="#"> {{ $user->name }}</a> </td>
                                        <td class="td">{{ $user->role }}</td>
                                        <td>
                                                {!! Form::open([ 'method' => 'POST', 'route' => ['add-user-discussion', $user->id, $discussion->id ]]) !!}
                                                {!! Form::submit('Ajouter', ['class' => 'btn btn-success', 'aria-hidden' => 'true', 'onclick' => 'return confirm(\'Vraiment ajouter cet utilisateur à la discussion ?\')']) !!}
                                                {!! Form::close() !!}
                                        </td>
                                </tr>
                        @endforeach
                                
                    @endif

                </table>
            
            @endif
            
            <!-- Champ de recherche -->
            {!! Form::open( [ 'method' => 'POST', 'route' =>  [ 'search-users-discussion', $discussion->id ] ] ) !!}
            
                <div class="input-group col-md-offset-3 col-md-5">
                    <input name="patern" type="text" class="form-control" value='{{ $patern }}' placeholder="Rechercher des utilisateurs">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="submit">Rechercher</button>
                    </span>
                </div>
            
            {!! Form::close() !!}
        </div>
    </div>
@endsection


