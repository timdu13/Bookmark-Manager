<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/">Bookmark Manager</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
        @can( 'create', 'App\Bookmark' ) <li {{ active( 'bookmark-create' ) }}><a href="{{ route( 'bookmark-create' ) }}">Nouveau Bookmark</a></li> @endcan
        @can( 'lister', 'App\Bookmark' ) <li {{ active( 'bookmark-list' ) }}><a href="{{ route( 'bookmark-list' ) }}">Liste des Bookmarks</a></li> @endcan
        @can( 'create', 'App\Group' ) <li {{ active( 'group-create' ) }}><a href="{{ route( 'group-create' ) }}">Nouveau groupe</a></li> @endcan
        @can( 'lister', 'App\Group' ) <li {{ active( 'group-list' ) }}><a href="{{ route( 'group-list' ) }}">Liste des Groupes</a></li> @endcan
        </ul>

        <ul class="nav navbar-nav navbar-right">
    @guest
            <li {{ active( 'login' ) }} ><a href="/login">Se connecter</a></li>
            <li {{ active( 'register' ) }} ><a href="/register">S'inscrire</a></li>
        
    @else
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                    {{ Auth::user()->name }} <span class="caret"></span>
                </a>

                <ul class="dropdown-menu">
                    <li>
                        <a href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                             document.getElementById('logout-form').submit();">
                            Se déconnecter
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                </ul>
            </li>
    @endguest
        </ul>
        
    </div><!-- /.navbar-collapse -->



  </div><!-- /.container-fluid -->
</nav>


<?php

function active( $name ) {
  return (Route::is( $name )) ? 'class=active' : '';
}
