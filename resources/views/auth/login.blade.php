@extends('template')

@section('content')



@include('errors')

    <div class="panel panel-default">

        <div class="panel-heading">Se connecter</div>


        <div class="panel-body">
            {!! Form::open( [ 'methode' => 'POST', 'route' => 'login' ] ) !!}
            
            <div class="form-group">
                {{ Form::label('email', "Adresse Email", ['class' => 'control-label']) }}
                
                {{ Form::email('email', null, ['class' => 'form-control']) }}
            </div>
            
            <div class="form-group">
                {{ Form::label('password', "Mot de passe", ['class' => 'control-label']) }}
                {{ Form::password('password', ['class' => 'form-control']) }}
            </div>

            <div class="form-group"><label>
                {{ Form::checkbox('remember', 'remember') }}
                Se rappeler de moi
            </div></label>

            <div class="form-group">
                {{ Form::submit('Se connecter', ['class' => 'btn btn-primary']) }}

                <a class="btn btn-link" href="{{ route('password.request') }}">
                    Mot de passe oublié ?
                </a>
            </div>
            
            
            {!! Form::close() !!}
        </div>
    </div>

@stop