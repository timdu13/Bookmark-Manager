<?php $patern = ( isset($patern) ) ? $patern : ''; ?>
@extends('template')


@section('content')


<h2> Liste des bookmarks du site </h2>

{!! Form::open(["methode"=> "POST", "route" => "bookmark-search"]) !!}

<a id="btn-ajouter" href="{{ route( 'bookmark-create' ) }}" class="btn btn-success col-md-offset-8 col-md-1">
    <span class="glyphicon glyphicon-plus"></span>
    <span id="txt-ajouter">Ajouter</span>
</a>

<div class="input-group col-md-offset-8 col-md-3">
    <input name="patern" type="text" class="form-control" value='{{ $patern }}' placeholder="Rechercher...">
    <span class="input-group-btn">
        <button class="btn btn-default" type="submit">Rechercher</button>
    </span>
</div>
<br />
{!! Form::close() !!}
<table class="table table-hover bg-default">
	

@foreach( $bookmarks as $bookmark )

	<tr>
		<td> <a class="col-md-10" href="{{ route( "bookmark-single", $bookmark->id ) }}"> {{ $bookmark->name }}</a> </td>
		<td class="td">{{ $bookmark->description }}</td>
		<td>
                    <a class="btn btn-primary" href="{{ route( 'bookmark-update', $bookmark->id ) }}">
                       <span class="glyphicon glyphicon-cog"></span>
                    </a>
		</td>
		<td>
                    {!! Form::open(['method' => 'DELETE', 'route' => ['bookmark-destroy', $bookmark->id]]) !!}
                    <button type="submit" class="btn btn-danger">
                       <span class="glyphicon glyphicon-remove"></span>
                    </button>
                    {!! Form::close() !!}
		</td>

	</tr>
@endforeach

</table>
		
@stop
