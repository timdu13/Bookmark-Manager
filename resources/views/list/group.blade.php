<?php 

$user = Auth::user();
$patern = ( isset($patern) ) ? $patern : '';
?>

@extends('template')

@section('content')


<h2> Liste des groupes du site </h2>


{!! Form::open(["methode"=> "POST", "route" => "group-search"]) !!}

<a id="btn-ajouter" href="{{ route( 'group-create' ) }}" class="btn btn-success col-md-offset-8 col-md-1">
    <span class="glyphicon glyphicon-plus"></span>
    <span id="txt-ajouter">Ajouter</span>
</a>

<div class="input-group col-md-offset-8 col-md-3">
    <input name="patern" type="text" class="form-control" value='{{ $patern }}' placeholder="Rechercher...">
    <span class="input-group-btn">
        <button class="btn btn-default" type="submit">Rechercher</button>
    </span>
</div>
<br />
{!! Form::close() !!}

<table class="table table-hover bg-default">
	

@foreach( $groups as $group )
	<tr>
            <td> <a href="{{ route( 'group-single', $group->id ) }}"> {{ $group->name }}</a> </td>
            <td>{{ $group->description }}</td>
        @if ( $user->role === 'admin' )
            <td>
                <a class="btn btn-primary" href="{{ route( 'group-update', $group->id ) }}">
                   <span class="glyphicon glyphicon-cog"></span>
                </a>
            </td>
            <td>
                {!! Form::open(['method' => 'DELETE', 'route' => ['group-destroy', $group->id]]) !!}

                <button type="submit" class="btn btn-danger">
                   <span class="glyphicon glyphicon-remove"></span>
                </button>

                {!! Form::close() !!}
            </td>
        @endif

	</tr>

@endforeach

</table>
		
@stop
