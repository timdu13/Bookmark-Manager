<?php 
use App\Discussion;
$user = Auth::user();

$patern = ( isset($patern) ) ? $patern : '';

?>

@extends('template')

@section('content')


<h2> Liste des utilisateurs du site </h2>

<table class="table table-hover bg-default">
	
@foreach( $users as $user )
    
	<tr>
            <td> <a href="#"> {{ $user->name }}</a> </td>
            <td>{{ $user->email }}</td>
            @if( Discussion::isInDiscussion( $user->id, $discussion->id ) )
                <td>
                    {!! Form::open(['method' => 'DELETE', 'route' => ['remove-user-discussion', $user->id, $discussion->id ] ]) !!}

                    <button type="submit" class="btn btn-danger">
                        Supprimer
                    </button>

                    {!! Form::close() !!}
                </td>
            @else
                <td>
                    {!! Form::open([ 'method' => 'POST', 'route' => ['add-user-discussion', $user->id, $discussion->id ] ]) !!}
                    <button class="btn btn-success">
                        Inviter
                    </button>
                    {!! Form::close() !!}
                </td>
            @endif

	</tr>

@endforeach

</table>
		
@stop
