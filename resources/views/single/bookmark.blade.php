
@extends('template')


@section('content')

<div class="panel panel-primary">
    <input id="id_bookmark" type="hidden" value="{{ $bookmark->id }}">
    <input id="id_user" type="hidden" value="{{ Auth::user()->id }}">
    <div class="panel-heading">
        <h2>{{ $bookmark->name }}</h2>
        <div class="flex-end">
            
            <div class="align-right" style="margin-right: 15px;">
                <div>Nombre de vote : <span id="nb_vote">{{ intval( $bookmark->nb_vote ) }}</span></div>
                <div>Nombre de cliques: <span id="id_clics">{{ intval( $bookmark->nb_vues ) }}</span></div>
            </div>
            
            <div class="btn-group">
            @auth
                <button id="button_vote" class="btn btn-success btn-hover" data-toggle="tooltip" data-placement="bottom" title="Vous avez déjà voté pour ce Boukmark">
                    <span class="glyphicon glyphicon-thumbs-up"></span>
                    <span class="cache-hover">J'aime</span>
                </button>
            @endauth
            @if( $canEdit )

                <a class="btn btn-primary btn-hover" href='{{ route('bookmark-update', $bookmark->id ) }}'>
                    <span class="glyphicon glyphicon-cog"></span>
                    <span class="cache-hover">Mettre à jour</span>
                </a>
                <button class="btn btn-danger btn-hover">
                    <span class="glyphicon glyphicon-remove"></span>
                    <span class="cache-hover">Supprimer</span>
                </button>
            @endif
            </div>

            
                
            
        </div>
    </div>
    
    <div class="panel-body">
        <p><strong>Description :</strong> {{ $bookmark->description }}</p>
        @if( $bookmark->type === 'url' )
        
            <strong>Url :</strong> <a href="{{ $bookmark->url }}" class="btn btn-link">{{ $bookmark->url }}</a>
        
        @elseif( $bookmark->type === 'text' )
        
            <p><strong>Contenus :</strong> {{ $bookmark->content }}</p>
        
        @endif
    </div>
</div>

@if( count( $comments ) == 0 )
<div class="well well-sm">
        Pas encore de commentaire...
</div>
@else

    @foreach( $comments as $comment)

    <div class="well well-sm">
        <div class="comment-head">{{ ucfirst( $comment->user->name ) }} a écrit le {{ $comment->createdAt }} :</div>
        {{ $comment->content }}
    </div>

    @endforeach
@endif

@auth
    {!! Form::open( [ 'route' => [ 'bookmark-add-comment', $bookmark->id ] ] ) !!}

    {{ Form::hidden( 'id_user', Auth::user()->id ) }}
    {{ Form::hidden( 'id_bookmark', $bookmark->id ) }}
    
    <div class="form-group" id="form-bm-content">
        {{ Form::label('content-comment', "Commentaire", ['class' => 'control-label']) }}
        {{ Form::textarea('content-comment', null, ['class' => 'form-control', 'rows' => '3']) }}
    </div>

    <div class="form-group">
        {{ Form::submit('Commenter', ['class' => 'btn btn-primary']) }}
    </div>


    {!! Form::close() !!}
@endauth

@stop

@section('scripts')


<script type="text/javascript">

    
    $("#button_vote").on("click", function() {
        $.ajax({
            url: '/addvote/bookmark',
            type: 'POST',
            data: 
            {
                id_bookmark: $("#id_bookmark").val(),
                id_user: $("#id_user").val()
        
            },
            success: function(retour) {
                if( retour === true ) {
                    let nb_vote = Number( $("#nb_vote").html() );
                    $("#nb_vote").html( nb_vote + 1 );
                }
                else {
                    alert('Vous avez déjà voté pour ce Bookmark');
                }
            }   
        });
    });

</script>

@stop