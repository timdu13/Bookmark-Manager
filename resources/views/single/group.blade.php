<?php
use App\Group;
$user = Auth::user();

/**
 * Discussion du groupe
 */
$group_dsc = $group->discussion()->first();

?>


@extends('template')

@section('aside-no-fix')

<!-- Action groupe -->
<div class="panel panel-danger">
    
    <div class="panel-heading">
        
        <h2> Gérer groupe </h2>
        
    </div>
    
    <div class="panel-body">
        
        <div class="list-group">
            <a href="{{ route( 'bookmark-create' ) }}" class="list-group-item">Publier un bookmark</a>
            <a href="{{ route( 'discussion-show', $group_dsc->id ) }}" class="list-group-item">Discussion</a>
            
            @if ( $user && $group->isAdmin($user->id) )
                
                <!-- Si ce n'est pas un groupe personnel -->
                @if( ! Group::isPersonalGroup($group->id) )
                    <a href="{{ route( 'discussion-edit', $group_dsc->id ) }}" class="list-group-item">Gérer la conversation de groupe</a>
                    <a href="#" class="list-group-item">Inviter un utilisateur</a>
                @endif

                <a href="{{ route( 'group-update', $group->id ) }}" class="list-group-item">Gérer mon groupe</a>
            
            @endif
            
        </div>
        
    </div>
    
</div>


<!-- Description -->
<div class="panel panel-success">
    
    <div class="panel-heading">
        
        <h2>Description du groupe</h2>
        
    </div>
    
    <div class="panel-body">
        
        <p>
            
            {{ $group->description }}
            
        </p>    
            
    </div>
    
</div>

@if ( ! Group::isPersonalGroup( $group->id ) )
<!-- Membres -->
<div class="panel panel-warning">
    
    <div class="panel-heading">
        
        <h2> Membres </h2>
        
    </div>
    
    <div class="panel-body">
        
        <div class="list-group">
            @foreach( $group->users as $user )
            
                <a href="#" class="list-group-item">{{ $user->name }}</a>
                    
            @endforeach
        </div>
        
    </div>
    
</div>
@endif

@endsection


@section('content')

<div class="panel panel-primary">
    
    <div class="panel-heading">

        <h1>{{ $group->name }}</h1>
        
    </div>
    
    
    <!-- Arborescence Bm -->
    <div class="panel-body">
        
        
        
    </div>
    
    <div class="panel-footer">
        
        
        
    </div>
    
</div>

@endsection


